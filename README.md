# README #

This is a simple Vagrantfile for Windows users who want to run a Hyper-V VM but cannot use Virtualbox because of conflicts with Docker Desktop.

## Vagrant

### What does it do? ###

* Sets up shared folder on host.
* Copies public key to `authorized_keys` on guest.

## Requirements ##
### Local Machine ###
* [Git](https://git-scm.com/)
* [Vagrant](https://www.vagrantup.com/)
* SSH key
* `ssh-agent` enabled on host
* Hyper-V enabled (this is required by Docker Desktop anyway)

### Remote Machine ###
* user with sudo privileges e.g. vagrant

### How do I get set up? ###

* Copy `vagrant.default.example.yml` to `vagrant.default.yml` and populate local values
* Create a file called `import_keys.pub`, populate with public ssh key and place in repository root.
* Create a folder in host home directory named "Share" e.g. "C:\User\username\Shared"
* Create Vagrant instance `vagrant up`

### Known issues ###

* CentOS not yet fully supported.

## Ansible

* `ansible-galaxy install -r requirements.yml`
* `ansible-playbook localhost.yml -i localhost`