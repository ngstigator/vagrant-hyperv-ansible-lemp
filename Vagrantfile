# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

ANSIBLE_PATH = __dir__ # absolute path to Ansible directory on host machine

require 'yaml'

# Read variables from file
vconfig = YAML.load_file("#{ANSIBLE_PATH}/vagrant.default.yml")

Vagrant.configure("2") do |config|

  config.vm.box = vconfig.fetch('vagrant_box')
  config.vm.provider "hyperv"

  config.vm.provider :hyperv do |h|
    h.vmname = vconfig.fetch('vagrant_hostname')
    h.memory = vconfig.fetch('vagrant_memory')
    h.cpus = vconfig.fetch('vagrant_cpus')
    h.linked_clone = true
    h.vm_integration_services = {
      guest_service_interface: true
      # CustomVMSRV: true
    }
  end

  # Optional SMB. Make sure to remove other synced_folder line too
  config.vm.synced_folder "C:\\Users\\" + vconfig.fetch('vagrant_hostuser') + "\\Shared", "/media/Shared", type: "smb", smb_username: vconfig.fetch('vagrant_smbusername'), smb_password: vconfig.fetch('vagrant_smbpassword')
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # SSH
  # Copy local public key(s) to guest
  config.vm.provision "file", source: "./import_keys.pub", destination: "/home/vagrant/.ssh/import_keys.pub"

  # append import key(s) to authorized_keys and remove duplicates
  $script = <<-SCRIPT
  sudo apt update
  sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1
  sudo apt install ansible -y
  cd /home/vagrant/.ssh
  cat import_keys.pub >> authorized_keys
  sort authorized_keys | uniq > public_keys.pub
  mv public_keys.pub authorized_keys
  chmod 644 authorized_keys
  rm *.pub
  SCRIPT

  config.vm.provision "shell" do |sh|
    sh.inline = $script
    sh.privileged = false
  end

  config.ssh.forward_agent = true

end
